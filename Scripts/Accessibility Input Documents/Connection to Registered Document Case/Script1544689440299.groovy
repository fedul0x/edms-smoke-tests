import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import func as func

String docContent = func.randomString(10)

String url = GlobalVariable.url_to.toString()

func.signin('registrator_1')

driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('document test/i_add_document'))

WebUI.click(findTestObject('document test/id_recipient_div'))

WebUI.setText(findTestObject('document test/input_id_sender-input'), 'Авиа-Л')

WebUI.click(findTestObject('document test/input_id_recipient_strong_Avia-L'))

WebUI.click(findTestObject('document test/textarea_content_label'))

WebUI.setText(findTestObject('document test/textarea_content'), docContent)

WebUI.click(findTestObject('document test/input_id_addressee-input'))

WebUI.setText(findTestObject('document test/input_id_addressee-input'), 'Полу')

WebUI.click(findTestObject('document test/Page_doc_create/div_addressee'))

WebUI.click(findTestObject('document test/input_id_responsible-input'))

WebUI.setText(findTestObject('document test/input_id_responsible-input'), 'Отве')

WebUI.click(findTestObject('document test/Page_doc_create/div_responsible'))

WebUI.click(findTestObject('document test/button_from_document_form_register_input')) //register

WebUI.navigateToUrl(url.concat('documents/inputdocuments/'))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + docContent) + '"]'))

document.click()

connect = driver.findElement(By.xpath("//a[contains(text(),'Связать')]"))

connect.click()

input_connect = driver.findElement(By.xpath("//a[contains(text(),'С входящим')]"))

input_connect.click()

String documentContent = docContent + func.randomString(10)

WebUI.click(findTestObject('document test/id_recipient_div'))

WebUI.setText(findTestObject('document test/input_id_sender-input'), 'Авиа-Л')

WebUI.click(findTestObject('document test/input_id_recipient_strong_Avia-L'))

WebUI.click(findTestObject('document test/textarea_content_label'))

WebUI.setText(findTestObject('document test/textarea_content'), documentContent)

WebUI.click(findTestObject('document test/input_id_addressee-input'))

WebUI.setText(findTestObject('document test/input_id_addressee-input'), 'Полу')

WebUI.click(findTestObject('document test/Page_doc_create/div_addressee'))

WebUI.click(findTestObject('document test/input_id_responsible-input'))

WebUI.setText(findTestObject('document test/input_id_responsible-input'), 'Отве')

WebUI.click(findTestObject('document test/Page_doc_create/div_responsible'))

WebUI.click(findTestObject('document test/button_from_document_form_register_input')) //register

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('registrator_1_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

documents = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(documents.size() != 0){
	throw new RuntimeException("Документ найден регистратором 1.1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('registrator_1_2')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

documents = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(documents.size() != 0){
	throw new RuntimeException("Документ найден регистратором 1.2")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('poluchatel_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!document){
	throw new RuntimeException("Документ недоступен адресату")
}

document.click()

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('otvetstvennyj_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!document){
	throw new RuntimeException("Документ недоступен ответственному")
}

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))
 
document.click()

connect = driver.findElement(By.xpath("//a[contains(text(),'поручение')]"))

connect.click()

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), documentContent)
WebUI.click(findTestObject('Page_Create_Assignments/label_Controller'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Испо')
WebUI.click(findTestObject('Page_Create_Assignments/strong_ispol'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_controller-input'), 'Контр')
WebUI.click(findTestObject('Page_Create_Assignments/strong_contr'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_date_of_execution'), '23.03.2019')
WebUI.click(findTestObject('Page_Create_Assignments/input__send'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('ispolnitel_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!document){
	throw new RuntimeException("Документ недоступен исполнителю поручения")
}

WebUI.navigateToUrl(url.concat('assignments2me/?is_filtering=true&description=' + documentContent))

assignment = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!assignment){
	throw new RuntimeException("Поручение недоступно исполнителю")
}

assignment.click()

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('kontroler_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/inputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!document){
	throw new RuntimeException("Документ недоступен контролеру поручения")
}

WebUI.navigateToUrl(url.concat('assignments/?is_filtering=true&description=' + documentContent))

assignment = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if (!assignment){
	throw new RuntimeException("Поручение недоступно контролеру")
}

assignment.click()

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)