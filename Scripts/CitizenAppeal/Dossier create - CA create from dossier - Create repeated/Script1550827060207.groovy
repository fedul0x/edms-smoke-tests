import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Keys as Keys
import func as func

String appeal_content = func.randomString(10)

String first_name = func.randomString(10)

String last_name = func.randomString(10)

String middle_name = func.randomString(10)

String url = GlobalVariable.url_to.toString()

String appeal_filtering_url = (url + 'documents/appeals/?is_filtering=true&content=') + appeal_content

String dossier_filtering_url = url + String.format('references/dossier/?is_filtering=true&last_name=%s&first_name=%s&middle_name=%s', 
    last_name, first_name, middle_name)

String appeal_xpath = ('//*[@id="table"]//td//a[text()="' + appeal_content) + '"]'

String dossier_xpath = ('//*[@id="table"]//td//a[text()="' + last_name) + '"]'

func.signin('veremyanina')

WebUI.delay(2)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'references/dossier/')

WebUI.delay(2)

WebUI.click(findTestObject('document test/i_add_document'))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_first_name'), first_name)

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_last_name'), last_name)

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_middle_name'), middle_name)

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_phone'), '88000000000')

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_email'), 'example@example.com')

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_apartment'), '2019')

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_street-input'), 'М')

WebUI.sendKeys(findTestObject('Appeals test/Dossier/input_id_street-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_house'), '17')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/Dossier/id_citizenship'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/Dossier/id_citizenship'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_city-input'), 'Вор')

WebUI.sendKeys(findTestObject('Appeals test/Dossier/input_id_city-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_district-input'), 'гор')

WebUI.sendKeys(findTestObject('Appeals test/Dossier/input_id_district-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_region-input'), 'Вор')

WebUI.sendKeys(findTestObject('Appeals test/Dossier/input_id_region-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.setText(findTestObject('Appeals test/Dossier/input_id_country-input'), 'Росс')

WebUI.sendKeys(findTestObject('Appeals test/Dossier/input_id_country-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.click(findTestObject('Appeals test/Dossier/button_save'))

WebUI.delay(2)

WebUI.navigateToUrl(dossier_filtering_url)

dossier = driver.findElement(By.xpath(dossier_xpath))

WebUI.delay(2)

dossier.click()

WebUI.delay(2)

WebUI.click(findTestObject('Appeals test/Dossier/click_create_citizen_appeal'))

WebUI.delay(2)

WebUI.setText(findTestObject('document test/input_id_responsible-input'), 'Отв')

WebUI.sendKeys(findTestObject('document test/input_id_responsible-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_method'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_method'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_type'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_type'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_nod'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_nod'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_addressee'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_addressee'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.setText(findTestObject('document test/textarea_content'), appeal_content)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

WebUI.delay(2)

WebUI.navigateToUrl(appeal_filtering_url)

appeal = driver.findElement(By.xpath(appeal_xpath))

WebUI.delay(2)

appeal.click()

WebUI.delay(2)

WebUI.click(findTestObject('Appeals Test/click_repeated_ca'))

WebUI.delay(2)

WebUI.setText(findTestObject('document test/input_id_responsible-input'), 'Отв')

WebUI.sendKeys(findTestObject('document test/input_id_responsible-input'), Keys.chord(Keys.DOWN))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_method'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_method'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_type'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_type'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_nod'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_nod'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_addressee'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('Appeals test/id_appeal_addressee'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.setText(findTestObject('document test/textarea_content'), 'repeated from' + appeal_content)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

