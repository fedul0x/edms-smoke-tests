import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import func as func
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import java.text.DateFormat as DateFormat
import java.text.SimpleDateFormat as SimpleDateFormat
import java.time.LocalDate as LocalDate
import java.time.LocalDateTime as LocalDateTime
import java.time.ZoneId as ZoneId

String assignmentDescription = func.randomString(10)

LocalDateTime date = LocalDateTime.now()

date = date.plusDays(7)

Date curDatePlusWeek = Date.from(date.atZone(ZoneId.systemDefault()).toInstant())

DateFormat dateFormat = new SimpleDateFormat('dd.MM.yyyy')

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Korchagin)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments'))

WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments work'))

WebUI.click(findTestObject('Page_Assignments_List_View/add_new_assignment_button'))

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentDescription)

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'мел')

WebUI.click(findTestObject('Page_Create_Assignments/strong_Mel'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_date_of_execution'), dateFormat.format(curDatePlusWeek))

WebUI.click(findTestObject('label_respons'))

WebUI.click(findTestObject('label_ control'))

WebUI.click(findTestObject('Page_Create_Assignments/input__send'))

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

String url = GlobalVariable.url_to.toString()

WebUI.navigateToUrl(url.concat('assignments2me/?is_filtering=true&description=&date_of_execution=&author=&controller=142&current_status__status=execution'))

WebUI.click(findTestObject('click_task'))

WebUI.click(findTestObject('click_change'))

WebUI.click(findTestObject('click_answer'))

WebUI.setText(findTestObject('textarea_answer'), 'ответ')

WebUI.click(findTestObject('input__execute'))

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Korchagin)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments'))

WebUI.navigateToUrl(url.concat('assignments/?is_filtering=true&description=&date_of_execution=&author=&implementer=62&current_status__status=executed'))

WebUI.click(findTestObject('link_assignment'))

WebUI.click(findTestObject('link_change'))

WebUI.setText(findTestObject('textarea_comment'), 'комментарий')

WebUI.click(findTestObject('input__rework'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

