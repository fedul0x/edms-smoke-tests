import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import func as func

String url = GlobalVariable.url_to.toString()

String documentContent = func.randomString(10)

String assignmentContent = (func.randomString(10) + ' для документа ') + documentContent

// New document creation and registration after
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('document test/i_add_document'))

WebUI.click(findTestObject('document test/label_firm'))

WebUI.setText(findTestObject('document test/input_id_sender-input'), 'Replin Fabrics фирма')

WebUI.delay(2)

WebUI.click(findTestObject('document test/label__desc'))

WebUI.setText(findTestObject('document test/textarea_content'), documentContent)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

WebUI.navigateToUrl(url.concat('documents/inputdocuments/'))

WebUI.delay(2)

document = driver.findElement(By.xpath(('//*[@id="table"]//td//a[text()="' + documentContent) + '"]'))

document.click()

WebUI.click(findTestObject('click_change'))

WebUI.click(findTestObject('document test/button_from_document_form_register_input'))

WebUI.closeBrowser()

//Creation new assignment by document
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Uporov)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/inputdocuments/')

WebUI.delay(2)

document = driver.findElement(By.xpath(('//*[@id="table"]//td//a[text()="' + documentContent) + '"]'))

document.click()

WebUI.click(findTestObject('document test/click_create_order'))

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentContent)

WebUI.click(findTestObject('Page_Create_Assignments/add_new_implementer_button'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Мел')

WebUI.click(findTestObject('Page_Create_Assignments/strong_Mel'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-1-c'), 'Корч')

WebUI.click(findTestObject('Page_Create_Assignments/strong_Korch'))

WebUI.click(findTestObject('Page_Create_Assignments/label_Controller'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_controller-input'), 'Корч')

WebUI.delay(2)

WebUI.click(findTestObject('Page_Create_Assignments/strong_Korch'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_date_of_execution'), '23.03.2019')

WebUI.click(findTestObject('Page_Create_Assignments/add_new_attachment_button'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('Page_Create_Assignments/input_assignment_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('click_sent'))

WebUI.closeBrowser()

//Execution assignment from melnikova
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'assignments2me/')

WebUI.delay(2)

assignment = driver.findElement(By.xpath(('//*[@id="table"]//td//a[text()="' + assignmentContent) + '"]'))

assignment.click()

WebUI.click(findTestObject('click_change'))

WebUI.delay(3)

WebUI.click(findTestObject('click_answer'))

WebUI.setText(findTestObject('textarea_answer'), 'ответ')

WebUI.click(findTestObject('document test/i_add_attachment'))

WebUI.uploadFile(findTestObject('document test/history_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('input__execute'))

WebUI.closeBrowser()

//Execution and close assignment from korchagin
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Korchagin)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'assignments2me/')

WebUI.delay(2)

assignment = driver.findElement(By.xpath(('//*[@id="table"]//td//a[text()="' + assignmentContent) + '"]'))

assignment.click()

WebUI.click(findTestObject('click_change'))

WebUI.delay(3)

WebUI.click(findTestObject('click_answer'))

WebUI.setText(findTestObject('textarea_answer'), 'ответ')

WebUI.click(findTestObject('document test/i_add_attachment'))

WebUI.uploadFile(findTestObject('document test/history_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('input__execute'))

WebUI.navigateToUrl(url + 'assignments/')

WebUI.delay(2)

assignment = driver.findElement(By.xpath(('//*[@id="table"]//td//a[text()="' + assignmentContent) + '"]'))

assignment.click()

WebUI.click(findTestObject('document test/a_remove_f_c'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

