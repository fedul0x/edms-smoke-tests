<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>id_appeal_addressee</name>
   <tag></tag>
   <elementGuidId>6deef074-fc0a-4f07-9d78-d2020b27a680</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;id_appeal_addressee_container&quot;]/div/input
</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;id_document_type&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;id_appeal_addressee_container&quot;]/div/input
</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;id_document_type&quot;]</value>
   </webElementXpaths>
</WebElementEntity>
