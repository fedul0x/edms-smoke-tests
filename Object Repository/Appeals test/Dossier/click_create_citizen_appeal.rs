<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click_create_citizen_appeal</name>
   <tag></tag>
   <elementGuidId>65613629-de5f-4dfb-bb58-6f355f3e2d7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/assignments2me/1202/change/' and (text() = 'Изменить' or . = 'Изменить')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[(text() = 'Создать обращение граждан' or . = 'Создать обращение граждан')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>waves-effect waves-light btn white-text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/assignments2me/1202/change/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Изменить</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;with-sidebar&quot;]/main[1]/div[@class=&quot;content&quot;]/div[@class=&quot;left-panel wide&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-content&quot;]/a[@class=&quot;waves-effect waves-light btn white-text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
