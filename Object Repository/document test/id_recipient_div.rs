<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>id_recipient_div</name>
   <tag></tag>
   <elementGuidId>5d1af80f-890f-46d8-94b3-6221390f4a72</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
        
        Корреспондент
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tab-1&quot;)/div[@class=&quot;layout-row row&quot;]/div[@class=&quot;col s12 m6&quot;]/div[@class=&quot;layout-column&quot;]/div[@class=&quot;section row&quot;]/div[@class=&quot;col s12&quot;]/div[@class=&quot;row&quot;]</value>
   </webElementProperties>
</WebElementEntity>
