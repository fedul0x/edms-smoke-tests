<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_remove_f_c</name>
   <tag></tag>
   <elementGuidId>75d79017-88e3-440c-9200-ec236f176c89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/main/div[2]/div/div/div[1]/a[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/assignments/terminate/213/' and (text() = 'Снять с контроля' or . = 'Снять с контроля')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>waves-effect waves-light btn white-text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/assignments/terminate/213/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Снять с контроля</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;with-sidebar&quot;]/main[1]/div[@class=&quot;content&quot;]/div[@class=&quot;left-panel wide&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-content&quot;]/a[@class=&quot;waves-effect waves-light btn white-text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
