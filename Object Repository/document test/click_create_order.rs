<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click_create_order</name>
   <tag></tag>
   <elementGuidId>8c1e9ee2-90f0-47cf-8f82-005c695b5ce7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'waves-effect waves-light btn white-text' and (text() = 'Создать поручение' or . = 'Создать поручение')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[(text() = 'Создать поручение' or . = 'Создать поручение')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>waves-effect waves-light btn white-text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/assignments/add_with_connected_object/471/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Создать поручение</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;with-sidebar&quot;]/main[1]/div[@class=&quot;content&quot;]/div[@class=&quot;left-panel wide&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-content&quot;]/a[@class=&quot;waves-effect waves-light btn white-text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
